import os
import glob
import sys
import platform
from PyQt5.QtWidgets import *
from PyQt5 import uic

user = glob.glob("*.uvguix.*")
ptx = glob.glob("*.uvoptx")
projx = glob.glob("*.uvprojx")

form_class = uic.loadUiType("rename.ui")[0]

class MyWindow(QMainWindow, form_class):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.clicked.connect(self.btn_clicked)

    def btn_clicekd(self):
        rename_text = self.textEdit.toPlainText()

        os.rename(user, rename_text + ".uvguix." + platform.node())
        os.rename(ptx, rename_text + ".uvoptx")
        os.rename(projx, rename_text + ".uvprojx")


#경로를 폴더를 설정해준다.

#.uvguix* ,.uvoptx, .uvprojx 파일 3개만 바꾸면 된다.
#.uv* 로 통일되니 그런식을 코딩해서 다 바꾼다.

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mywindow = MyWindow()
    mywindow.show()
    app.exec_()